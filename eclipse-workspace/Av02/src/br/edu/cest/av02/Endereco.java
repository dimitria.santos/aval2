package br.edu.cest.av02;
import br.edu.cest.av02.Cidade;

public class Endereco {
	
	private String cidade;
	private String logradouro;
	
	public Endereco(){
		
	}	

	public String getCidade() {
		return cidade;
	}

	public void setCidade(String cidade) {
		this.cidade = cidade;
	}

	public String getLogradouro() {
		return logradouro;
	}

	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}
	
    @Override
    public String toString() {
    	return "---Endere�o---{"
    			+ "Cidade: " + cidade +
    			", Lograduro: " + logradouro+
    			'}';	
    }
    	
    public Endereco(String cidade, String logradouro) {
    	this.cidade = cidade;
    	this.logradouro = logradouro;
    }
	
}

