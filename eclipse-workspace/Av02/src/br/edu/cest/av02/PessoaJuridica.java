package br.edu.cest.av02;

public class PessoaJuridica extends Pessoa{
	
	private String nome;
	private String endereco;
	private String cnpj;
	
	public PessoaJuridica() {
		
	}

	public String getNome() {
		return nome;
	}
	
	public PessoaJuridica(String nome, String cnpj) {
		this.nome = nome;
		this.cnpj = cnpj;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEndereco() {
		return this.endereco.toString();
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}
	
	
}
