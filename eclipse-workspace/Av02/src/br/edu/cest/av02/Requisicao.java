package br.edu.cest.av02;

public class Requisicao {
	
	public String nome;
	
	public Requisicao(String nome) {
		this.nome = nome;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	
	
}