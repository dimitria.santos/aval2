package br.edu.cest.av02;

public class Funcionario extends PessoaFisica {
	
	private String nome;
	private String cargo;
	private double salario;
	
	public Funcionario (String nome, String cpf, String cargo, double salario)
	
	{
		super(nome, cpf);
		this.cargo = cargo;
		this.salario = salario;
   }

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCargo() {
		return cargo;
	}

	public void setCargo(String cargo) {
		this.cargo = cargo;
	}

	public double getSalario() {
		return salario;
	}

	public void setSalario(double salario) {
		this.salario = salario;
	}
	

}
