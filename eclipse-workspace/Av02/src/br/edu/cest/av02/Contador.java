package br.edu.cest.av02;

public class Contador {
	
	private int i = 0;
	public static Contador c = new Contador();
	
	public Contador() {
		
	}
	
	public static Contador getInstance() {
		
		if (c == null) c = new
				Contador();
		return c;
	}
	
	public int proximo() {
		return i++;
	}
	
}


