package br.edu.cest.av02;

public class PessoaFisica extends Pessoa {
	private String nome;
	private String sexo;
	private String cpf;
	private String endereco;
	
	public PessoaFisica() {
		
	}

	public String getNome() {
		return nome;
	}
	
	public PessoaFisica(String nome, String cpf) {
		this.nome = nome;
		this.cpf = cpf;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getSexo() {
		return sexo;
	}

	public void setSexo(String sexo) {
		this.sexo = sexo;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getEndereco() {
		return this.endereco.toString();
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}
}
