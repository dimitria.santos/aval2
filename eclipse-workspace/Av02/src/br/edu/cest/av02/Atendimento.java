package br.edu.cest.av02;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Atendimento {
	
	private Date data;
	private int numero;
	
	private List <Requisicao> requisicao;
	
	public void aRequisicao(String nome) {
		this.requisicao.add (new Requisicao(nome));
	}
	
	public void fAtedentimento() {
		requisicao = null;
	}

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public int getNumero() {
		return numero;
	}

	public void setNumero(int numero) {
		this.numero = numero;
	}
	
	public Atendimento(Date data, Integer numero) {
        this.data = data;
        this.numero = numero;
        this.requisicao = new ArrayList<>();
	}
	
	 @Override
	    public String toString() {
	        return "Atendimento{" +
	                "Data: " + data +
	                ", N�mero: " + numero +
	                " Requisi��o: " + requisicao +
	                '}';
	        
	}
}

